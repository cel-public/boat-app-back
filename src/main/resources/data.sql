INSERT INTO users (id, username, email, role, password) VALUES
(1001, 'user',    'user@ptc.com',     'ROLE_USER',    '$2y$10$mQs4D7aLFW/jPDzDT9vK8.DlvQ45JJBKgF4worE1o2fe2ZLdfwfty'),
(1002, 'user2',   'user2@ptc.com',    'ROLE_USER',    '$2y$10$mQs4D7aLFW/jPDzDT9vK8.DlvQ45JJBKgF4worE1o2fe2ZLdfwfty'),
(1000, 'admin',   'admin@ptc.com',    'ROLE_ADMIN',   '$2y$10$mQs4D7aLFW/jPDzDT9vK8.DlvQ45JJBKgF4worE1o2fe2ZLdfwfty');

INSERT INTO boat(id, name, description, location, size, owner_id) VALUES
(1001, 'Étoile Marine', 'Un voilier élégant au design moderne, parfait pour les croisières en mer.', 'Côte d''Azur, France', 40, 1001),
(1002, 'Aventure Aquatique', 'Un catamaran robuste conçu pour les expéditions en eau profonde, équipé de technologies de pointe.', 'Îles Galápagos, Équateur', 60, 1001),
(1003, 'Brisant des Vagues', 'Un yacht luxueux avec des intérieurs somptueux, idéal pour les escapades en haute mer.', 'Miami, États-Unis', 80, 1001),
(1004, 'Mystère Marin', 'Un sous-marin de recherche équipé de technologies de pointe pour explorer les profondeurs océaniques.', 'Baie de Monterey, Californie', 25, 1002),
(1005, 'Horizon Infini', 'Un voilier classique adapté aux longues traversées océaniques, offrant une expérience authentique de navigation.', 'Les Caraïbes', 45, 1002),
(1006, 'Perle des Mers', 'Un yacht de luxe avec un équipage dédié, offrant une expérience exclusive de croisière côtière.', 'Saint-Tropez, France', 70, 1002),
(1007, 'Tempête Silencieuse', 'Un bateau de pêche sportive équipé des dernières technologies pour les passionnés de pêche en haute mer.', 'Îles Féroé', 55, 1000);

