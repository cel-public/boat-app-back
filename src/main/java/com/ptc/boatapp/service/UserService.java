package com.ptc.boatapp.service;

import com.ptc.boatapp.models.User;
import com.ptc.boatapp.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> find(Principal principal) {
        return userRepository.findByUsername(principal.getName());
    }
}
