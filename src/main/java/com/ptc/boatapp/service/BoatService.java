package com.ptc.boatapp.service;

import com.ptc.boatapp.models.Boat;
import com.ptc.boatapp.models.ERole;
import com.ptc.boatapp.models.User;
import com.ptc.boatapp.repository.BoatRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BoatService {

    private final BoatRepository boatRepository;

    public BoatService(BoatRepository boatRepository) {
        this.boatRepository = boatRepository;
    }

    public Boat findBoat(Long requestedId, User user) {
        if (user.getRole().equals(ERole.ROLE_ADMIN)) {
            return boatRepository.findById(requestedId).orElse(null);
        } else {
            return boatRepository.findByIdAndOwner(requestedId, user);
        }

    }

    public boolean existsByIdAndOwner(Long id, User user) {
        if (user.getRole().equals(ERole.ROLE_ADMIN)) {
            return boatRepository.existsById(id);
        } else {
            return boatRepository.existsByIdAndOwner(id, user);
        }
    }

    public List<Boat> getBoats(User user) {

        List<Boat> page;
        if (user.getRole().equals(ERole.ROLE_ADMIN)) {
            page = boatRepository.findAll();
        } else {
            page = boatRepository.findByOwner(user);

        }

        return page;
    }

    public Boat save(Boat boat) {
        return boatRepository.save(boat);
    }

    public void deleteById(Long id) {
        boatRepository.deleteById(id);
    }

}
