package com.ptc.boatapp.models;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
