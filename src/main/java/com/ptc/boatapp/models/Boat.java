package com.ptc.boatapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "boat")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Boat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column
    String name;

    @Column
    String description;

    @Column
    String location;

    @Column
    Double size;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "owner_id")
    User owner;
}
