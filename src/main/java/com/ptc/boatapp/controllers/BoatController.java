package com.ptc.boatapp.controllers;

import com.ptc.boatapp.models.Boat;
import com.ptc.boatapp.models.User;
import com.ptc.boatapp.service.BoatService;
import com.ptc.boatapp.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/boats")
public class BoatController {
    private final BoatService boatService;
    private final UserService userService;

    private BoatController(BoatService boatService, UserService userService) {
        this.boatService = boatService;
        this.userService = userService;
    }

    @GetMapping("/{requestedId}")
    private ResponseEntity<Boat> findById(@PathVariable Long requestedId, Principal principal) {
        User user = userService.find(principal).get();
        Boat boat = boatService.findBoat(requestedId, user);
        if (boat != null) {
            return ResponseEntity.ok(boat);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    private ResponseEntity<Void> createBoat(@RequestBody Boat newBoatRequest, UriComponentsBuilder ucb, Principal principal) {
        User user = userService.find(principal).get();
        Boat boatWithOwner = new Boat(
                null,
                newBoatRequest.getName(),
                newBoatRequest.getDescription(),
                newBoatRequest.getLocation(),
                newBoatRequest.getSize(),
                user
        );
        Boat savedBoat = boatService.save(boatWithOwner);
        URI locationOfNewBoat = ucb
                .path("boats/{id}")
                .buildAndExpand(savedBoat.getId())
                .toUri();
        return ResponseEntity.created(locationOfNewBoat).build();
    }


    @GetMapping
    private ResponseEntity<List<Boat>> findAll(Principal principal) {
        User user = userService.find(principal).get();
        return ResponseEntity.ok(boatService.getBoats(user));
    }

    @PutMapping("/{requestedId}")
    private ResponseEntity<Void> putBoat(@PathVariable Long requestedId, @RequestBody Boat boatUpdate, Principal principal) {
        User user = userService.find(principal).get();
        Boat boat = boatService.findBoat(requestedId, user);
        if (boat != null) {
            Boat updatedBoat = new Boat(
                    requestedId,
                    boatUpdate.getName(),
                    boatUpdate.getDescription(),
                    boatUpdate.getLocation(),
                    boatUpdate.getSize(),
                    user
            );
            boatService.save(updatedBoat);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<Void> deleteBoat(@PathVariable Long id, Principal principal) {
        User user = userService.find(principal).get();
        if (boatService.existsByIdAndOwner(id, user)) {
            boatService.deleteById(id);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();

    }


}
