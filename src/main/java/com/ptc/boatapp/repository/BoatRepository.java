package com.ptc.boatapp.repository;

import com.ptc.boatapp.models.Boat;
import com.ptc.boatapp.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BoatRepository extends JpaRepository<Boat, Long> {

    Boat findByIdAndOwner(Long id, User owner);

    List<Boat> findByOwner(User owner);

    boolean existsByIdAndOwner(Long id, User owner);

}
