package com.ptc.boatapp.controllers;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.ptc.boatapp.dtos.request.LoginRequest;
import com.ptc.boatapp.models.Boat;
import net.minidev.json.JSONArray;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BoatControllerTests {

    private static final Map<String, HttpHeaders> mapUserHeaders = new HashMap<>();
    @Autowired
    TestRestTemplate restTemplate;

    private HttpHeaders getHttpHeadersWithAuthCookieForUserTest(String username) {
        HttpHeaders httpHeaders = mapUserHeaders.get(username);
        if (httpHeaders == null) {
            httpHeaders = loginAndGetAuthenticationCookie(username, "password");
            mapUserHeaders.put(username, httpHeaders);
        }
        return httpHeaders;
    }

    private HttpHeaders loginAndGetAuthenticationCookie(String username, String password) {
        LoginRequest loginRequest = new LoginRequest(username, password);

        ResponseEntity<String> response = restTemplate
                .postForEntity("/authentication/login", loginRequest, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);


        List<String> setCookie = response.getHeaders().get("Set-Cookie");
        assertThat(setCookie).isNotEmpty();

        String cookie = setCookie.get(0).split(";")[0];
        assertThat(cookie).isNotBlank();

        return AuthentificationControllerTests.getHttpHeadersWithAuthenticationCookie(cookie);
    }

    @Test
    void shouldReturnABoatWhenDataIsSaved() {
        ResponseEntity<String> response = restTemplate.exchange(
                "/boats/99",
                HttpMethod.GET,
                new HttpEntity<>(getHttpHeadersWithAuthCookieForUserTest("usertest")),
                String.class
        );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        DocumentContext documentContext = JsonPath.parse(response.getBody());
        Number id = documentContext.read("$.id");
        assertThat(id).isEqualTo(99);

        Double size = documentContext.read("$.size");
        assertThat(size).isEqualTo(187.5);
    }

    @Test
    void shouldNotReturnABoatWithAnUnknownId() {

        ResponseEntity<String> response = restTemplate
                .exchange(
                        "/boats/1000",
                        HttpMethod.GET,
                        new HttpEntity<>(getHttpHeadersWithAuthCookieForUserTest("usertest")),
                        String.class
                );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isBlank();
    }

    @Test
    @DirtiesContext
    void shouldCreateANewBoat() {

        Boat newBoat = new Boat(null, "name", "description", "location", 250.00, null);
        ResponseEntity<Void> createResponse = restTemplate.exchange(
                "/boats",
                HttpMethod.POST,
                new HttpEntity<>(newBoat, getHttpHeadersWithAuthCookieForUserTest("usertest")),
                Void.class
        );
        assertThat(createResponse.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        URI locationOfNewBoat = createResponse.getHeaders().getLocation();
        ResponseEntity<String> getResponse = restTemplate.exchange(
                locationOfNewBoat,
                HttpMethod.GET,
                new HttpEntity<>(getHttpHeadersWithAuthCookieForUserTest("usertest")),
                String.class
        );
        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

        DocumentContext documentContext = JsonPath.parse(getResponse.getBody());
        Number id = documentContext.read("$.id");
        Double size = documentContext.read("$.size");

        assertThat(id).isNotNull();
        assertThat(size).isEqualTo(250.00);
    }

    @Test
    void shouldReturnAllBoatsWhenListIsRequested() {

        ResponseEntity<String> response = restTemplate.exchange(
                "/boats",
                HttpMethod.GET,
                new HttpEntity<>(getHttpHeadersWithAuthCookieForUserTest("usertest")),
                String.class
        );
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        DocumentContext documentContext = JsonPath.parse(response.getBody());
        int boatCount = documentContext.read("$.length()");
        assertThat(boatCount).isEqualTo(3);

        JSONArray amounts = documentContext.read("$..size");
        assertThat(amounts).containsExactlyInAnyOrder(187.5, 1.00, 150.00);
    }

    @Test
    void shouldReturnAllBoatsWhenListIsRequestedWithAdminUser() {

        ResponseEntity<String> response = restTemplate.exchange(
                "/boats",
                HttpMethod.GET,
                new HttpEntity<>(getHttpHeadersWithAuthCookieForUserTest("admin")),
                String.class
        );
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        DocumentContext documentContext = JsonPath.parse(response.getBody());
        int boatCount = documentContext.read("$.length()");
        assertThat(boatCount).isGreaterThan(3);
    }

    @Test
    void shouldNotReturnABoatWhenUsingNoAuthentication() {

        ResponseEntity<String> response = restTemplate.exchange(
                "/boats/99",
                HttpMethod.GET,
                new HttpEntity<>(null),
                String.class
        );
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    void shouldNotAllowAccessToBoatsTheyDoNotOwn() {

        ResponseEntity<String> response = restTemplate.exchange(
                "/boats/102",
                HttpMethod.GET,
                new HttpEntity<>(getHttpHeadersWithAuthCookieForUserTest("usertest")),
                String.class
        );
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    @DirtiesContext
    void shouldUpdateAnExistingBoat() {

        Boat boatUpdate = new Boat(null, "name", "description", "location", 19.99, null);
        ResponseEntity<Void> response = restTemplate.exchange(
                "/boats/99",
                HttpMethod.PUT,
                new HttpEntity<>(boatUpdate, getHttpHeadersWithAuthCookieForUserTest("usertest")),
                Void.class
        );
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        ResponseEntity<String> getResponse = restTemplate.exchange(
                "/boats/99",
                HttpMethod.GET,
                new HttpEntity<>(getHttpHeadersWithAuthCookieForUserTest("usertest")),
                String.class
        );
        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        DocumentContext documentContext = JsonPath.parse(getResponse.getBody());
        Number id = documentContext.read("$.id");
        Double size = documentContext.read("$.size");
        assertThat(id).isEqualTo(99);
        assertThat(size).isEqualTo(19.99);
    }

    @Test
    void shouldNotUpdateABoatThatDoesNotExist() {

        Boat unknownBoat = new Boat(null, "name", "description", "location", 19.99, null);
        ResponseEntity<Void> response = restTemplate.exchange(
                "/boats/99999",
                HttpMethod.PUT,
                new HttpEntity<>(unknownBoat, getHttpHeadersWithAuthCookieForUserTest("usertest")),
                Void.class
        );
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void shouldNotUpdateABoatThatIsOwnedBySomeoneElse() {
        Boat user2Boat = new Boat(null, "name", "description", "location", 333.33, null);
        ResponseEntity<Void> response = restTemplate.exchange(
                "/boats/102",
                HttpMethod.PUT,
                new HttpEntity<>(user2Boat, getHttpHeadersWithAuthCookieForUserTest("usertest")),
                Void.class
        );
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    @DirtiesContext
    void shouldDeleteAnExistingBoat() {
        ResponseEntity<Void> deleteResponse = restTemplate.exchange(
                "/boats/99",
                HttpMethod.DELETE,
                new HttpEntity<>(getHttpHeadersWithAuthCookieForUserTest("usertest")),
                Void.class
        );
        assertThat(deleteResponse.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        ResponseEntity<String> getResponse = restTemplate.exchange(
                "/boats/99",
                HttpMethod.GET,
                new HttpEntity<>(getHttpHeadersWithAuthCookieForUserTest("usertest")),
                String.class
        );
        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void shouldNotDeleteABoatThatDoesNotExist() {
        ResponseEntity<Void> deleteResponse = restTemplate.exchange(
                "/boats/99999",
                HttpMethod.DELETE,
                new HttpEntity<>(getHttpHeadersWithAuthCookieForUserTest("usertest")),
                Void.class
        );
        assertThat(deleteResponse.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void shouldNotAllowDeletionOfBoatsTheyDoNotOwn() {

        ResponseEntity<Void> deleteResponse = restTemplate.exchange(
                "/boats/102",
                HttpMethod.DELETE,
                new HttpEntity<>(getHttpHeadersWithAuthCookieForUserTest("usertest")),
                Void.class
        );
        assertThat(deleteResponse.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

        ResponseEntity<String> getResponse = restTemplate.exchange(
                "/boats/102",
                HttpMethod.GET,
                new HttpEntity<>(getHttpHeadersWithAuthCookieForUserTest("usertest2")),
                String.class
        );
        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}
