package com.ptc.boatapp.controllers;

import com.ptc.boatapp.dtos.request.LoginRequest;
import com.ptc.boatapp.dtos.request.SignupRequest;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import java.util.List;

import static com.ptc.boatapp.configurations.JWTUtils.JWT_COOKIE_NAME;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthentificationControllerTests {

    public static final String USERNAME = "new";
    public static final String PASSWORD = "password";
    private static String cookie;

    @Autowired
    TestRestTemplate restTemplate;

    public static HttpHeaders getHttpHeadersWithAuthenticationCookie(String authenticationCookie) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", authenticationCookie);
        return headers;
    }

    @Test
    void shouldReturnForbiddenWhenLoginWithBadCredentials() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("test");
        loginRequest.setPassword("anotherpassword");
        ResponseEntity<String> response = restTemplate
                .postForEntity("/authentication/login", loginRequest, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    @Order(1)
    void shouldReturnSuccessWhenSignupNewUser() {
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername(USERNAME);
        signupRequest.setEmail(USERNAME + "@ptc.com");
        signupRequest.setPassword(PASSWORD);
        ResponseEntity<String> response = restTemplate
                .postForEntity("/authentication/signup", signupRequest, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    @Order(2)
    void shouldReturnSuccessAndCookieWhenLoginNewUser() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(USERNAME);
        loginRequest.setPassword(PASSWORD);
        ResponseEntity<String> response = restTemplate
                .postForEntity("/authentication/login", loginRequest, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);


        List<String> setCookie = response.getHeaders().get("Set-Cookie");
        assertThat(setCookie).isNotEmpty();
        cookie = setCookie.get(0).split(";")[0];

        assertThat(cookie).isNotBlank().contains(JWT_COOKIE_NAME + "=");
    }

    @Test
    @Order(3)
    void shouldReturnNoContentWhenLogout() {
        HttpHeaders headers = getHttpHeadersWithAuthenticationCookie(cookie);
        ResponseEntity<String> response2 = restTemplate
                .exchange("/authentication/logout", HttpMethod.POST, new HttpEntity<>(headers), String.class);
        assertThat(response2.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

}
