INSERT INTO users(id, username, email, role, password) VALUES
(500, 'usertest', 'usertest@email.com', 'ROLE_USER', '$2y$10$mQs4D7aLFW/jPDzDT9vK8.DlvQ45JJBKgF4worE1o2fe2ZLdfwfty'),
(600, 'usertest2', 'usertest2@email.com', 'ROLE_USER', '$2y$10$mQs4D7aLFW/jPDzDT9vK8.DlvQ45JJBKgF4worE1o2fe2ZLdfwfty'),
(700, 'admintest', 'admintest@email.com', 'ROLE_ADMIN', '$2y$10$mQs4D7aLFW/jPDzDT9vK8.DlvQ45JJBKgF4worE1o2fe2ZLdfwfty');

INSERT INTO boat(id, name, description, location, size, owner_id) VALUES
(99, 'Étoile Marine', 'Un voilier', 'Côte d''Azur, France', 187.5, 500),
(100, 'Aventure Aquatique', 'Un catamaran', 'Îles Galápagos, Équateur', 1.00, 500),
(101, 'Brisant des Vagues', 'Un yacht luxueux', 'Miami, États-Unis', 150.00, 500),
(102, 'Mystère Marin', 'Un sous-marin', 'Baie de Monterey, Californie', 200.00, 600);
