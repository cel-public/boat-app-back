FROM amazoncorretto:17

COPY target/*.jar /app.jar

EXPOSE 5000

ENTRYPOINT ["java"]
CMD ["-Dspring.profiles.active=prod", "-jar", "/app.jar"]
