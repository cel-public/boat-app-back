# Boat App Backend

Bienvenue dans la documentation de l'application back end de Boat App.
Cette application est développée en utilisant Java 17 et Spring Boot.

L'application expose des APIs permettant l'authentification des utilisateurs, la génération de tokens JWT et d'effectuer
des opérations CRUD sur des bateaux.

## Prérequis

Avant de commencer, assurez-vous d'avoir installé les éléments suivants sur votre système :

- Java 17
- Maven 3.X

## Configuration

1. Clonez ce dépôt sur votre machine locale :

    ```bash
    git clone https://gitlab.com/chafikel/boatapp.git
    ```

2. Naviguez vers le répertoire du projet :

    ```bash
    cd boatapp
    ```

3. Build du projet avec Maven :

    ```bash
    mvn clean install
    ```

## Lancement de l'Application

1. Exécutez l'application à l'aide de la commande suivante :

    ```bash
    java -jar target/boat-app-0.0.1-SNAPSHOT.jar
    ```

2. L'application sera disponible à l'adresse [http://localhost:8080](http://localhost:8080).
3. Le point d'entrée suivant est accessible sans
   authentification : [http://localhost:8080/test](http://localhost:8080/test).

## CI/CD

L'application est automatiquement déployée sur un serveur AWS Beanstalk via des pipelines CI/CD.
Vous pouvez accéder à la version déployée à l'adresse
suivante : [http://boat-app-back.chafikel.fr/test](http://boat-app-back.chafikel.fr/test).

## Base de Données H2

L'application utilise une base de données H2 préconfigurée avec des données d'exemple. La base de données contient trois
utilisateurs et sept bateaux. Voici les détails :

### Utilisateurs

1. **Username:** user, **Email:** user@ptc.com, **Rôle:** ROLE_USER
2. **Username:** user2, **Email:** user2@ptc.com, **Rôle:** ROLE_USER
3. **Username:** admin, **Email:** admin@ptc.com, **Rôle:** ROLE_ADMIN

Ils ont tous le même mot de passe : **password**

### Bateaux

1. **Étoile Marine** appartient à **user**
2. **Aventure Aquatique** appartient à **user**
3. **Brisant des Vagues** appartient à **user**

4. **Mystère Marin** appartient à **user2**
5. **Horizon Infini** appartient à **user2**
6. **Perle des Mers** appartient à **user2**

7. **Tempête Silencieuse** appartient à **admin**

